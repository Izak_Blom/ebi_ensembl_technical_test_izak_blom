import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRadioModule } from '@angular/material/radio';
import { GbInputComponent } from './gene-browser/gb-input/gb-input.component';
import { GeneBrowserComponent } from './gene-browser/gene-browser.component';
import { GeneDiagramComponent } from './gene-browser/gene-diagram/gene-diagram.component';
import { ExonDiagramComponent } from './gene-browser/gene-diagram/transcript-diagram/exon-diagram/exon-diagram.component';
import { IntronDiagramComponent } from './gene-browser/gene-diagram/transcript-diagram/intron-diagram/intron-diagram.component';
import { TranscriptDiagramComponent } from './gene-browser/gene-diagram/transcript-diagram/transcript-diagram.component';
import { HistoryComponent } from './gene-browser/history/history.component';
import { SortComponent } from './gene-browser/sort/sort.component';
import { MatTooltipModule } from '@angular/material/tooltip';


@NgModule({
  declarations: [
    GbInputComponent,
    GeneBrowserComponent,
    GeneDiagramComponent,
    TranscriptDiagramComponent,
    ExonDiagramComponent,
    IntronDiagramComponent,
    HistoryComponent,
    SortComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    // Angular Material imports
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatProgressBarModule,
    MatListModule,
    MatRadioModule,
    MatTooltipModule

  ],
  exports: [GeneBrowserComponent]
})
export class GeneBrowserModule { }
