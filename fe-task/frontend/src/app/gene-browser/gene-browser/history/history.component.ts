import { EventEmitter } from '@angular/core';
import { Component, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

import { Gene } from 'src/app/models/gene';
import { CacheService } from 'src/app/services/cache.service';

@Component({
  selector: 'history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit, OnChanges {

  /**
   * The Stable ID of the currently selected gene
   */
  @Input() stableId: string;

  stableIdsHistory: string[];

  /**
   * Emits the selected Gene when a new Gene is selected
   */
  @Output() selected = new EventEmitter<Gene>();

  constructor(
    private cacheService: CacheService
  ) { }

  ngOnInit() {
    // refresh cache
    this.stableIdsHistory = this.cacheService.getCacheIds();
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.stableId.currentValue !== changes.stableId.previousValue) {

      // refresh cache if current stableId changed
      this.stableIdsHistory = this.cacheService.getCacheIds();
    }
  }

  onSelect(stableId: string) {
    this.selected.emit(this.cacheService.getCacheItem(stableId));
  }

}
