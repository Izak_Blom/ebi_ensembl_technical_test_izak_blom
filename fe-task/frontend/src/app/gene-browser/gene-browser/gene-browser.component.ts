import { Component, OnInit } from '@angular/core';
import { Gene } from 'src/app/models/gene';
import { CacheService } from 'src/app/services/cache.service';
import { LookupService } from 'src/app/services/lookup.service';

@Component({
  selector: 'gene-browser',
  templateUrl: './gene-browser.component.html',
  styleUrls: ['./gene-browser.component.scss']
})
export class GeneBrowserComponent implements OnInit {

  loading = false;

  gene: Gene;

  errMsg: string;



  constructor(
    private lookupService: LookupService,
    private cacheService: CacheService
  ) { }

  ngOnInit(): void {
    // init to the first item present in the cache
    this.gene = this.cacheService.getFirst();
  }

  fetchGene(stableId: string) {
    this.reset();
    // first check the cache
    const cached = this.cacheService.getCacheItem(stableId);
    if (cached) {
      this.gene = cached;
    } else {
      this.loading = true;
      this.lookupService.getGeneByStableId(stableId).subscribe(result => {
        this.gene = result;
      }, (error: Error) => {

        this.errMsg = error.message;

      }).add(() => this.loading = false);
    }

  }

  reset() {
    this.gene = null;
    this.errMsg = null;
  }

}
