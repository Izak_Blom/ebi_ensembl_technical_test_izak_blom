import { Component, Input, OnInit } from '@angular/core';
import { Exon } from 'src/app/models/exon';

@Component({
  selector: 'exon-diagram',
  templateUrl: './exon-diagram.component.html',
  styleUrls: ['./exon-diagram.component.scss']
})
export class ExonDiagramComponent implements OnInit {

  /**
   * The Exon representation to be displayed
   */
  @Input() exon: Exon;

  /**
   * The width in pixels of the containing gene diagram
   */
  @Input() geneWidth: number;

  left: number;
  right: number;

  constructor() { }

  ngOnInit() {
    // calculate the offsets in pixels relative to the containing gene's pixel width
    this.left = (this.exon.offset_start_relative_to_parent * this.geneWidth);
    this.right = (this.exon.offset_end_relative_to_parent * this.geneWidth);
  }

}
