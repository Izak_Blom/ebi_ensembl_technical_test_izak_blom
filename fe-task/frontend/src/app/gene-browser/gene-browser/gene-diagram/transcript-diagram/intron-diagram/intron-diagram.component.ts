import { Component, Input, OnInit } from '@angular/core';
import { Exon } from 'src/app/models/exon';
import { Intron } from 'src/app/models/intron';

@Component({
  selector: 'intron-diagram',
  templateUrl: './intron-diagram.component.html',
  styleUrls: ['./intron-diagram.component.scss']
})
export class IntronDiagramComponent implements OnInit {

  /**
   * The Intron representation to be displayed
   */
  @Input() intron: Intron;

  /**
   * The width in pixels of the containing gene diagram
   */
  @Input() geneWidth: number;

  left: number;
  right: number;

  constructor() { }

  ngOnInit() {
    // calculate the offsets in pixels relative to the containing gene's pixel width
    this.left = (this.intron.offset_start_relative_to_parent * this.geneWidth);
    this.right = (this.intron.offset_end_relative_to_parent * this.geneWidth);
  }

}
