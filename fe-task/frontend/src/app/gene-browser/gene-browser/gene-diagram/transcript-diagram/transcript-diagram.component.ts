import { Component, Input, OnInit } from '@angular/core';
import { Transcript } from 'src/app/models/transcript';

@Component({
  selector: 'transcript-diagram',
  templateUrl: './transcript-diagram.component.html',
  styleUrls: ['./transcript-diagram.component.scss']
})
export class TranscriptDiagramComponent implements OnInit {

  /**
   * The transcript to display
   */
  @Input() transcript: Transcript;

  /**
   * The width in pixels of the containing gene diagram
   */
  @Input() geneWidth: number;

  left: number;
  right: number;

  constructor() { }

  ngOnInit() {
    // calculate the offsets in pixels relative to the containing gene's pixel width
    this.left = (this.transcript.offset_start_relative_to_parent * this.geneWidth);
    this.right = (this.transcript.offset_end_relative_to_parent * this.geneWidth);
  }

}
