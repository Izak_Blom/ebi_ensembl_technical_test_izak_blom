import { animate, state, style, transition, trigger } from '@angular/animations';
import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Gene } from 'src/app/models/gene';

@Component({
  selector: 'gene-diagram',
  templateUrl: './gene-diagram.component.html',
  styleUrls: ['./gene-diagram.component.scss'],
  animations: [
    trigger('insertRemoveAnimation', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('400ms ease-in-out', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        animate('400ms ease-in-out', style({ opacity: 0 }))
      ])
    ]),
  ]
})
export class GeneDiagramComponent implements OnInit, OnChanges {

  /**
   * The representation of the gene to display
   */
  @Input() gene: Gene;

  width = 1100;


  sort: 'asc' | 'desc' = 'asc';

  animate = false;

  constructor(
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.gene.currentValue !== changes.gene.previousValue) {
      // Update the transcript sort order if the gene changes
      this.updateSort();
    }
  }

  private updateSort() {
    const transcripts = this.gene.transcripts.slice();
    this.gene.transcripts = [];
    if (this.animate) {
      this.cdr.detectChanges(); // trigger animation
    }
    if (this.sort === 'asc') {
      transcripts.sort((a, b) => a.seq_region_length > b.seq_region_length ? 1 : -1);
    } else {
      transcripts.sort((a, b) => a.seq_region_length < b.seq_region_length ? 1 : -1);
    }
    this.gene.transcripts = transcripts;
  }

  onSort(sort: 'asc' | 'desc') {
    this.sort = sort;
    this.animate = true;
    this.updateSort();
    this.animate = false;
  }

}
