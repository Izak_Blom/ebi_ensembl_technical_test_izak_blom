import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'sort',
  templateUrl: './sort.component.html',
  styleUrls: ['./sort.component.scss']
})
export class SortComponent implements OnInit {

  sort: 'asc' | 'desc' = 'asc';

  /**
   * Emits the selected sort order when changed
   */
  @Output() newSort = new EventEmitter<'asc' | 'desc'>();

  constructor() { }

  ngOnInit() {
  }

  onChange() {
    this.newSort.emit(this.sort);
  }

}
