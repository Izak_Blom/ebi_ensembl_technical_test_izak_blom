import { ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'gb-input',
  templateUrl: './gb-input.component.html',
  styleUrls: ['./gb-input.component.scss']
})
export class GbInputComponent implements OnInit, OnDestroy {

  /**
   * Emits a string containing the stable ID input
   * once valid input was submitted
   */
  @Output() newStableId = new EventEmitter<string>();

  /**
   * Emits an event whenever the stable ID input is changing
   */
  @Output() changing = new EventEmitter<boolean>();

  stableIdControl: FormControl;

  private subs: Subscription[] = [];

  constructor(
  ) {
    this.stableIdControl = new FormControl(null, [Validators.required, Validators.pattern(/^ENSG\d{11}$/)]);
  }

  ngOnInit(): void {
    this.subs.push(this.stableIdControl.valueChanges.subscribe(() => {
      this.changing.emit(true);
    }));
  }

  ngOnDestroy() {
    // Unsubscribe from form control events
    for (const sub of this.subs) {
      sub.unsubscribe();
    }
  }

  getErrorMessage() {
    if (this.stableIdControl.hasError('required')) {
      return 'Stable ID input required';
    } else if (this.stableIdControl.hasError('pattern')) {
      return 'Stable ID input is invalid. A stable ID should start with \'ENSG\' followed by 11 digits.';
    } else {
      return 'Invalid';
    }
  }

  submit() {
    this.newStableId.emit(this.stableIdControl.value);
  }

}
