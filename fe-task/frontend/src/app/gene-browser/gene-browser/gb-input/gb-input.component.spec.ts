/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GbInputComponent } from './gb-input.component';

describe('GbInputComponent', () => {
  let component: GbInputComponent;
  let fixture: ComponentFixture<GbInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GbInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
