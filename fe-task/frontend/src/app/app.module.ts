import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { GeneBrowserModule } from './gene-browser/gene-browser.module';
import { LookupService } from './services/lookup.service';
import { HttpClientModule } from '@angular/common/http';
import { CacheService } from './services/cache.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule, // animations
    MatToolbarModule,
    GeneBrowserModule,
    HttpClientModule
  ],
  providers: [
    LookupService,
    CacheService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
