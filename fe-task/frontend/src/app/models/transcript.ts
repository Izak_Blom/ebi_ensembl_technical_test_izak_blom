import { Exon } from './exon';
import { Intron } from './intron';
import { SequenceFeature } from './sequenceFeature';

export interface Transcript extends SequenceFeature {
  exons: Exon[];
  display_name: string;
  gene_id?: number;
  transcript_id?: number;

  introns?: Intron[];
}
