import { SequenceFeature } from './sequenceFeature';

export interface Exon extends SequenceFeature {
  transcript_id?: number;
}
