
export interface ApiErrorResponse {
  error: { message: string, errors?: any };
}
