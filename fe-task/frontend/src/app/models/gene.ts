import { SequenceFeature } from './sequenceFeature';
import { Transcript } from './transcript';

export interface Gene extends SequenceFeature {
  display_name: string;
  description: string;
  transcripts: Transcript[];
}
