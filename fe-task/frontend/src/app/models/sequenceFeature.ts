

export interface SequenceFeature {
  stable_id: string;
  seq_region_start: number;
  seq_region_end: number;
  seq_region_length: number;

  offset_start_relative_to_parent?: number;
  offset_end_relative_to_parent?: number;
}
