import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Gene } from '../models/gene';
import { catchError, map } from 'rxjs/operators';
import { CacheService } from './cache.service';
import { Intron } from '../models/intron';

@Injectable({
  providedIn: 'root'
})
/**
 * Handles API requests to the lookup API service and mapping of results for frontend purposes
 */
export class LookupService {

  constructor(
    private http: HttpClient,
    private cacheService: CacheService
  ) { }

  /**
   * Request the gene with provided Stable ID from the API service
   */
  public getGeneByStableId(stableId) {
    return this.http.get<Gene>(`${environment.apiEndpointUrl}/lookup/gene/${stableId}`).pipe(
      map(gene => {
        if (gene) {
          // map calculated fields
          gene = this.mapGene(gene);

          // save item to cache
          this.cacheService.putItem(gene);

        }
        return gene;
      }),
      catchError((error: HttpErrorResponse) => {
        const body = error.error;

        let errMsg = 'An unknown error ocurred';
        if (body.error && body.error.message) {
          errMsg = body.error.message;
        }
        throw new Error(errMsg);
      }));
  }

  /**
   * Maps the gene representation with size offsets for components relative to the overall gene size
   */
  mapGene(gene: Gene) {
    const geneSize = gene.seq_region_length;
    const geneStart = gene.seq_region_start;
    // calculate transcript size percentages
    for (const transcript of gene.transcripts) {
      transcript.offset_start_relative_to_parent = (transcript.seq_region_start - geneStart) / geneSize;
      transcript.offset_end_relative_to_parent = (transcript.seq_region_end - geneStart) / geneSize;

      const tStart = transcript.seq_region_start;

      transcript.introns = [];


      for (let i = 0; i < transcript.exons.length; i++) {
        const exon = transcript.exons[i];
        exon.offset_start_relative_to_parent = (exon.seq_region_start - tStart) / geneSize;
        exon.offset_end_relative_to_parent = (exon.seq_region_end - tStart) / geneSize;

        if (transcript.exons.length > 1 && i < transcript.exons.length - 1) {
          const intron: Intron = {
            seq_region_start: exon.seq_region_end,
            seq_region_end: transcript.exons[i + 1].seq_region_start,
            seq_region_length: null,
            stable_id: null,
            offset_start_relative_to_parent: null,
            offset_end_relative_to_parent: null
          };
          intron.seq_region_length = intron.seq_region_end - intron.seq_region_start;
          intron.offset_start_relative_to_parent = (intron.seq_region_start - tStart) / geneSize;
          intron.offset_end_relative_to_parent = (intron.seq_region_end - tStart) / geneSize;

          transcript.introns.push(intron);
        }

      }



    }
    return gene;
  }

}


