import { Injectable } from '@angular/core';
import { Gene } from '../models/gene';

@Injectable({
  providedIn: 'root'
})
/**
 * Handles local storage persistance
 */
export class CacheService {

  private readonly CACHE_SIZE = 5;
  private readonly KEY = 'GB_CACHE';

  constructor() {
  }

  /**
   * Save a Gene item to localstorage. Inserts the item in the beginning of the cached list
   * Will remove duplicate genes
   */
  putItem(gene: Gene) {
    const cache = this.readCache();

    const idxFound = cache.findIndex(c => c.stable_id === gene.stable_id);

    if (idxFound !== -1) {
      cache.splice(idxFound, 1);
    }


    if (cache.length === this.CACHE_SIZE) {
      cache.pop();
    }

    cache.unshift(gene);

    this.saveCache(cache);
  }

  /**
   * Get the stable ID's of the genes currently in the cache
   */
  getCacheIds(): string[] {
    const cache = this.readCache();

    return cache.map(c => c.stable_id).slice();
  }

  /**
   * Get a gene item by stable ID from cache
   */
  getCacheItem(stableId: string): Gene {
    const cache = this.readCache();
    return cache.find(c => c.stable_id === stableId);
  }

  /**
   * Get the first item in the cache
   */
  getFirst() {
    const cache = this.readCache();
    if (cache.length > 0) {
      return cache[0];
    }
    return null;
  }

  private saveCache(genes: Gene[]) {
    localStorage.setItem(this.KEY, JSON.stringify(genes));
  }

  private readCache(): Gene[] {
    const cache = localStorage.getItem(this.KEY);
    if (cache) {
      return JSON.parse(cache);
    }

    return [];
  }





}
