# Frontend Task

## Author

Izak Blom

izakjblom@gmail.com


## Table of Contents  

1. [Live Demo](#live-demo)   
2. [Run the App](#run-the-app)
5. [Libraries and Frameworks](#libaries-and-frameworks)

## Live demo

A live version of the app can be accessed here:

<https://fe-task-ebi-yzlm5zv5xa-nw.a.run.app/>

## Run the app

### Option 1 - Run as a docker  container (recommended)

1. `cd` into this directory
2. `docker-compose up -d` to run a detached container
3. Once the image has been built and runs in a local container (fe_app_izak_blom), open <http://localhost> in a browser window to view the running app
4. Note, this will build the app with a configuration containing this live backend URL as an API endpoint: <https://be-task-ebi-yzlm5zv5xa-nw.a.run.app/>
5. `docker-compose down` to terminate the container once done

### Option 2 - Run natively for development

1. Ensure node and npm are installed on the local development environment <https://nodejs.org/en/download/>
2. Ensure the backend task is running locally as a container. Refer to the backend task [instructions](./../../be-task/backend/README.md) for instructions on starting the backend application.
3. `cd` into this directory
4. `npm install`
5. `npm start`
6. Once the app has compiled and is running in the local terminal, navigate to <http://localhost:4200> in a browser window to view the app
7. Note, this will build the app with a configuration containing a local URL as an API endpoint: <https://localhost:3000/>


## Libaries and Frameworks

### 1. [Angular](https://angular.io/)

* Provides modular structuring of code into templates, components, services
* Components can easily be re-used within and between projects
* Typescript allows compile-time type safety and language features not provided by Javascript
* Excellent documentation
* Framework maintained by Google
* Ahead of time compilation - typescript and angular HTML templates are compiled into javascript during the build process as opposed to in the client browser
* Provides dependency injection, enhancing code decoupling
* Single Page Application (SPA) implementation provides a rich user experience

### 2. [Angular Material](https://material.angular.io/)

* Material design enables building interfaces familiar to end-users
* Extensive library of UI components for a variety of use-cases

