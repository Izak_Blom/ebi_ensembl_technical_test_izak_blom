# EBI01682 Technical Assignment #

Implementation of the EBI01682 technical assignment as part of the application for the role Ensembl Web Full Stack Developer at EBI, Hinxton UK. 

## Author

Izak Blom
izakjblom@gmail.com 

## Tasks Implemented

### [1. Backend Task](./be-task/backend/README.md)

Only the Part 1 Basic was implemented. 
 Please view the documentation and assignment question answers for this task [here](./be-task/backend/README.md).

### [2. Frontend Task](./fe-task/frontend/README.md)

The entire task with extensions was implemented. 
Please view the documentation for this task [here](./fe-task/frontend/README.md).

### Setup

Please view the setup instructions for each task under their respective folders:

1. [Frontend](./fe-task/frontend/README.md)
2. [Backend](./be-task/backend/README.md)

 
 