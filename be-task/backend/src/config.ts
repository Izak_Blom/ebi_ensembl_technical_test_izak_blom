import dotenv from 'dotenv';
import { ConfigException } from './exceptions/configException';




dotenv.config({ path: `./${process.env.NODE_ENV}.env` });

export class Config {

    private static instance: Config;

    public static get getInstance(): Config {
        if (!this.instance) {
            this.instance = new Config();
        }
        return this.instance;
    }



    public readonly PORT: number;

    public readonly DB_CONFIG: {
        DB_USERNAME: string,
        DB_NAME: string,
        DB_HOST: string,
        DB_PASSWORD: string
    };


    private constructor() {

        const nodeEnv = process.env.NODE_ENV;
        console.log('NODE_ENV is ' + nodeEnv);
        if (!nodeEnv) {
            throw new ConfigException(this.getError('NODE_ENV'));
        }

        this.validateEnvironment();

        this.PORT = Number.parseInt(process.env.PORT as string, 10);

        this.DB_CONFIG = {
            DB_USERNAME: process.env.DB_USERNAME as string,
            DB_HOST: process.env.DB_HOST as string,
            DB_NAME: process.env.DB_NAME as string,
            DB_PASSWORD: process.env.DB_PASSWORD as string
        };


    }

    private validateEnvironment() {
        if (process.env.PORT === undefined) {
            throw new ConfigException(this.getError('PORT'));
        }

        if (process.env.DB_USERNAME === undefined) {
            throw new ConfigException(this.getError('DB_USERNAME'));
        }

        if (process.env.DB_HOST === undefined) {
            throw new ConfigException(this.getError('DB_HOST'));
        }

        if (process.env.DB_NAME === undefined) {
            throw new ConfigException(this.getError('DB_NAME'));
        }

        if (process.env.DB_PASSWORD === undefined) {
            throw new ConfigException(this.getError('DB_PASSWORD'));
        }



    }

    private getError(envVarName: string) {
        return `${envVarName} env variable not defined`;
    }
}

