export class ConfigException extends Error {
    constructor(public message: string) {
        super(message);

        Object.setPrototypeOf(this, ConfigException.prototype);
    }
}
