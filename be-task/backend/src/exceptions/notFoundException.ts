export class NotFoundException extends Error {
    constructor(public message: string) {
        super(message);

        Object.setPrototypeOf(this, NotFoundException.prototype);
    }
}
