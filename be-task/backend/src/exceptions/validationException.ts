export class ValidationException extends Error {
    constructor(public errors: any) {
        super();

        Object.setPrototypeOf(this, ValidationException.prototype);
    }
}
