export class QueryException extends Error {
    constructor(public message: string) {
        super(message);

        Object.setPrototypeOf(this, QueryException.prototype);
    }
}
