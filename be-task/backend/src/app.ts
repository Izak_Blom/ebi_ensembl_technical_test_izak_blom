

import express = require('express');
import { DbService } from './data/db.service';
import { Router } from './routes/router';
import cors from 'cors';

/**
 * Test database connectivity
 */
async function initDatabase() {
    try {
        const connection = await DbService.getInstance.getConnection();
        console.log('DATABASE CONNECTION SUCCESSFUL');
        connection.end();

    } catch (error) {
        console.error('DATABASE CONNECTION FAILURE. TERMINATING');
        process.kill(process.pid, 'SIGTERM');
    }

}



const app: express.Application = express();

app.use(cors());

const router = new Router(app);

initDatabase();


export = app;

