import express = require('express');
import { param, validationResult } from 'express-validator';
import { GeneQueries } from '../data/queries/gene.queries';
import { NotFoundException } from '../exceptions/notFoundException';
import { QueryException } from '../exceptions/queryException';
import { ValidationException } from '../exceptions/validationException';
import { ErrorResponse } from '../models/errorResponse';

const router = express.Router();



/**
 * @swagger
 * /lookup/gene/{stable_id}:
 *  get:
 *      description: Returns the coordinates of a gene identified by a stable ID, along with its transcripts and the exons for each transcript
 *      tags: [Lookup]
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: stable_id
 *          description: Stable ID of the gene to retrieve
 *          in: path
 *          required: true
 *          schema:
 *           type: string
 *      responses:
 *        200:
 *          description: Object containing the basic location information for the gene, its transcripts and the exons of each transcript
 *          content:
 *           application/json:
 *            schema:
 *              $ref: '#/components/schemas/Gene'
 *        500:
 *          description: Object containing error description(s)
 *          content:
 *           application/json:
 *            schema:
 *              $ref: '#/components/schemas/ErrorResponse'
 */
router.get('/gene/:stable_id?',
    param('stable_id')
        .matches(/^ENSG\d{11}$/)
        .withMessage('stable_id should start with \'ENSG\' followed by an 11 digit number'),
    async (req, res) => {
        try {

            const valResult = validationResult(req);
            if (!valResult.isEmpty()) {
                throw new ValidationException(valResult.array());
            }

            const stableId = req.params.stable_id;

            const result = await GeneQueries.getInstance.GetGeneTranscriptsAndExonsByStableId(stableId);


            if (!result) {
                throw new NotFoundException('Gene not found');
            }

            res.send(result);

        } catch (error) {

            if (error instanceof ValidationException) {
                res.status(400).json(new ErrorResponse('Invalid request', error.errors));
            } else if (error instanceof QueryException) {
                res.status(500).json(new ErrorResponse('The data for the specified gene could not be retrieved'));
            } else if (error instanceof NotFoundException) {
                res.status(404).json(new ErrorResponse('The specified gene was not found'));
            } else {
                console.log(error);
                res.status(500).json(new ErrorResponse('An unknown error ocurred'));
            }

        }
    });



export = router;
