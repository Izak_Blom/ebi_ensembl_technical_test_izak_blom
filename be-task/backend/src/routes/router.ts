import healthRoutesController from './health.routes.controller';
import lookupRoutesController from './lookup.routes.controller';
import documentationRoutesController from './documentation.routes.controller';

import express = require('express');

// Map routes to controller functions
export class Router {
    constructor(app: express.Application) {
        app.use('/lookup', lookupRoutesController);
        app.use('/health', healthRoutesController);
        app.use('/api-docs', documentationRoutesController);
        app.use('/', documentationRoutesController);
    }
}
