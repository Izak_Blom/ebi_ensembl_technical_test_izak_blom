import express = require('express');
import swaggerUi from 'swagger-ui-express';
import swaggerJSDoc from 'swagger-jsdoc';
import { swaggerDocument } from '../swagger/swaggerDocument';






const router = express.Router();

const options = {
    swaggerDefinition: swaggerDocument,
    apis: ['**/*.ts'],
};

const swaggerSpec = swaggerJSDoc(options);

router.use('/', swaggerUi.serve);
router.get('/', swaggerUi.setup(swaggerSpec));


export = router;



