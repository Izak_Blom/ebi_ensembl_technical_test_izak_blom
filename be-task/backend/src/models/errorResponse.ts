/**
 * @swagger
 * components:
 *  schemas:
 *    ErrorResponse:
 *      type: object
 *      properties:
 *       error:
 *        type: object
 *        description: Contains the error descriptions
 *        properties:
 *         message:
 *          type: string
 *          description: Error message description
 *         errors:
 *          type: object
 *          description: Additional errors related to the request
 *      example:
 *        message: Invalid request
 *        errors: [{"value": "ENSG0000157764",  "msg": "stable_id should start with 'ENSG' followed by an 11 digit number","param": "stable_id","location": "params"}]
 */
export class ErrorResponse {
    public error: { message: string, errors?: any };

    constructor(message: string, errors?: any) {
        this.error = { message, errors };
    }
}
