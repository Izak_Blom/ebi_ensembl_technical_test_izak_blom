import { SequenceFeature } from "./sequenceFeature";

/**
 * @swagger
 * components:
 *  schemas:
 *    Exon:
 *      allOf: 
 *       - $ref: '#/components/schemas/SequenceFeature'
 */

export interface Exon extends SequenceFeature {
    transcript_id?: number;
}
