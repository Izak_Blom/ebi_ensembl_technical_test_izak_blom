import { Exon } from "./exon";
import { SequenceFeature } from "./sequenceFeature";

/**
 * @swagger
 * components:
 *  schemas:
 *    Transcript:
 *      allOf: 
 *       - $ref: '#/components/schemas/SequenceFeature'
 *       - type: object
 *         properties:
 *          display_name:
 *           type: string
 *           description: The label for the transcript
 *          exons:
 *           type: array
 *           items: 
 *            $ref: '#/components/schemas/Exon'
 *      example:
 *        display_name: BRAF-204
 *        stable_id: ENST00000496384
 *        seq_region_start: 140719327
 *        seq_region_end: 140726516
 *        seq_region_length: 205482
 *        exons: [ { "stable_id": "ENSE00003823941", "seq_region_start": 25648011, "seq_region_end": 25648356, "seq_region_length": 344 }, ]
 */
export interface Transcript extends SequenceFeature {
    exons: Exon[];
    display_name: string;
    gene_id?: number;
    transcript_id?: number;
}
