import { SequenceFeature } from "./sequenceFeature";
import { Transcript } from "./transcript";

/**
 * @swagger
 * components:
 *  schemas:
 *    Gene:
 *      allOf: 
 *       - $ref: '#/components/schemas/SequenceFeature'
 *       - type: object
 *         properties:
 *          display_name:
 *           type: string
 *           description: The label for the gene
 *          description:
 *           type: string
 *           description: Description of the gene
 *          transcripts:
 *           type: array
 *           items: 
 *            $ref: '#/components/schemas/Transcript'
 *      example:
 *        display_name: BRAF
 *        description: B-Raf proto-oncogene, serine/threonine kinase
 *        stable_id: ENSG00000157764
 *        seq_region_start: 140719327
 *        seq_region_end: 140924929
 *        seq_region_length: 205601 
 *        transcripts: [ { "display_name": "SLC34A2-207", "stable_id": "ENST00000645788", "seq_region_start": 25648011, "seq_region_end": 25676769, "seq_region_length": 28757, "exons": [ { "stable_id": "ENSE00003823941", "seq_region_start": 25648011, "seq_region_end": 25648356, "seq_region_length": 344 }, ] } ]
 *   
 */
export interface Gene extends SequenceFeature {
    display_name: string;
    description: string;
    transcripts: Transcript[];
}
