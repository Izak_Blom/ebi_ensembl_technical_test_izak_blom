
/**
 * @swagger
 * components:
 *  schemas:
 *    SequenceFeature:
 *      type: object
 *      properties: 
 *       stable_id:
 *          type: string
 *          description: Stable ID of the feature
 *       seq_region_start:
 *         type: integer
 *         description: Start coordinate of the feature
 *       seq_region_end:
 *         type: integer
 *         description: End coordinate of the feature
 *       seq_region_length:
 *         type: integer 
 *         description: Length of the feature
 *      example: 
 *        stable_id: ENSG00000157764
 *        seq_region_start: 140719327
 *        seq_region_end: 140924929
 *        seq_region_length: 205601
 */

export interface SequenceFeature {
    stable_id: string;
    seq_region_start: number;
    seq_region_end: number;
    seq_region_length: number;
}
