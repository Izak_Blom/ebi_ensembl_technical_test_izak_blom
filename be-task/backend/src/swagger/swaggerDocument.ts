
export const swaggerDocument = {
    openapi: '3.0.0',
    info: {
        version: '1.0.0',
        title: 'Gene Lookup API',
        description: 'API endpoint for retrieving information about genes, their transcripts and exons in those transcripts.',
    },
};
