import mysql = require('mysql');
import { Config } from '../config';



export class DbService {

    private static instance: DbService;

    public static get getInstance(): DbService {
        if (!this.instance) {
            this.instance = new DbService();
        }
        return this.instance;
    }

    public getConnection() {
        return new Promise<mysql.Connection>((resolve, reject) => {

            const config = Config.getInstance;

            const connection = mysql.createConnection({
                host: config.DB_CONFIG.DB_HOST,
                user: config.DB_CONFIG.DB_USERNAME,
                password: config.DB_CONFIG.DB_PASSWORD,
                database: config.DB_CONFIG.DB_NAME,
                multipleStatements: true
            });

            connection.connect((err) => {
                if (err) {
                    reject(err);
                }
                resolve(connection);
            });
        });
    }

    public disconnect(connection: mysql.Connection) {
        if (connection) {
            connection.end();
        }
    }

}
