
import { Connection } from "mysql";
import { Config } from "../../config";
import { QueryException } from "../../exceptions/queryException";
import { Exon } from "../../models/exon";
import { Gene } from "../../models/gene";
import { Transcript } from "../../models/transcript";
import { DbService } from "../db.service";

export class GeneQueries {

    private static instance: GeneQueries;

    public static get getInstance(): GeneQueries {
        if (!this.instance) {
            this.instance = new GeneQueries();
        }
        return this.instance;
    }



    public async GetGeneTranscriptsAndExonsByStableId(stable_id: string): Promise<Gene> {

        let connection;

        const DB_NAME = Config.getInstance.DB_CONFIG.DB_NAME;

        try {

            connection = await DbService.getInstance.getConnection();


            const sql = `
                SELECT
                    xref.display_label as 'display_name',
                    xref.description as 'description',
                    gene.stable_id,
                    gene.seq_region_start,
                    gene.seq_region_end,
                    (gene.seq_region_end - gene.seq_region_start - 1) as 'seq_region_length'
                    FROM ${DB_NAME}.gene
                    LEFT JOIN ${DB_NAME}.xref ON ${DB_NAME}.xref.xref_id = ${DB_NAME}.gene.display_xref_id
                    WHERE gene.stable_id = ${connection.escape(stable_id)};
                SELECT
                    xref.display_label as 'display_name',
                    transcript.stable_id,
                    transcript.seq_region_start,
                    transcript.seq_region_end,
                    (transcript.seq_region_end - transcript.seq_region_start - 1) as 'seq_region_length',
                    transcript.transcript_id
                    FROM ${DB_NAME}.transcript
                    LEFT JOIN ${DB_NAME}.gene ON ${DB_NAME}.gene.gene_id = ${DB_NAME}.transcript.gene_id
                    LEFT JOIN ${DB_NAME}.xref ON ${DB_NAME}.xref.xref_id = ${DB_NAME}.transcript.display_xref_id
                    WHERE gene.stable_id = ${connection.escape(stable_id)}
                    ORDER BY transcript.seq_region_start;
                SELECT
                    exon.stable_id,
                    exon.seq_region_start,
                    exon.seq_region_end,
                    (exon.seq_region_end - exon.seq_region_start - 1) as 'seq_region_length',
                    exon_transcript.transcript_id
                    FROM ${DB_NAME}.exon
                    LEFT JOIN ${DB_NAME}.exon_transcript ON ${DB_NAME}.exon_transcript.exon_id = ${DB_NAME}.exon.exon_id
                    LEFT JOIN ${DB_NAME}.transcript ON ${DB_NAME}.exon_transcript.transcript_id = ${DB_NAME}.transcript.transcript_id
                    LEFT JOIN ${DB_NAME}.gene ON ${DB_NAME}.gene.gene_id = ${DB_NAME}.transcript.gene_id
                    WHERE gene.stable_id = ${connection.escape(stable_id)}
                    ORDER BY exon.seq_region_start;
            `;

            const rows = await this.executeSql(connection, sql);

            let gene: Gene = null;

            if (rows.length > 0 && rows[0].length > 0) {
                const geneRows: Gene[] = rows[0];
                const transcriptsRows: Transcript[] = rows[1];
                const exonRows: Exon[] = rows[2];

                gene = geneRows[0];
                gene.transcripts = transcriptsRows;

                for (const transcript of gene.transcripts) {
                    transcript.exons = exonRows.filter(exon => exon.transcript_id === transcript.transcript_id);
                }

                gene.transcripts = gene.transcripts.map(transcript => {
                    delete transcript.transcript_id;
                    transcript.exons = transcript.exons.map(exon => {
                        delete exon.transcript_id;
                        return exon;
                    });
                    return transcript;
                });
            }

            return gene;

        } catch (error) {
            console.log(error);
            throw new QueryException('Error encountered in GetGeneTranscriptsAndExonsByStableId');
        } finally {
            await DbService.getInstance.disconnect(connection);
        }

    }

    private executeSql(connection: Connection, sql: string) {
        return new Promise<any[]>((resolve, reject) => {
            connection.query({ sql }, (err, rows, fields) => {
                if (err) {
                    reject(err);
                }

                if (rows) {
                    resolve(rows);
                }
            });
        });
    }

}
