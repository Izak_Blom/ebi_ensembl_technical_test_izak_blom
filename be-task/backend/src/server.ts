import app from './app';
import { Config } from "./config";


const server = app.listen(Config.getInstance.PORT, () => {
    console.log(`Server started on PORT ${Config.getInstance.PORT}`);
});

process.on('SIGTERM', () => {
    server.close(() => {
        console.log('Process terminated');
    });
});
