
import { it, afterAll, beforeAll, describe, jest } from '@jest/globals';
import dotenv from 'dotenv';
import { Server } from 'http';
import request from 'supertest';
import express from 'express';
import { Router } from '../src/routes/router';





describe('lookup.routes.controller.test', () => {


    let server: Server;
    let env;

    beforeAll(() => {

        env = Object.assign({}, process.env);

        process.env.NODE_ENV = 'development';

        // Load the development configuration
        dotenv.config({ path: `./${process.env.NODE_ENV}.env` });

        const app: express.Application = express();

        const router = new Router(app);

        server = app.listen(3000, () => { });

    });

    afterAll((done) => {
        process.env = env;


        server.close(done);

    });



    it("response 200 '/'", (done) => {

        request(server)
            .get('/')
            .expect(200, done);
    });


    it("response 200 '/health/'", (done) => {

        request(server)
            .get('/health/')
            .expect(200, done);
    });



    it("response 400 '/lookup/gene/'", (done) => {

        const expectedBody = {
            "error": {
                "message": "Invalid request",
                "errors": [
                    {
                        "msg": "stable_id should start with 'ENSG' followed by an 11 digit number",
                        "param": "stable_id",
                        "location": "params"
                    }
                ]
            }
        };

        request(server)
            .get('/lookup/gene/')
            .expect(400, expectedBody, done);
    });





    it("response 404 non-existant parameter '/lookup/gene/ENSG10000157764'", (done) => {
        jest.setTimeout(20000);

        const expectedBody = {
            "error": {
                "message": "The specified gene was not found"
            }
        };

        request(server)
            .get('/lookup/gene/ENSG10000157764')
            .expect(404, expectedBody, done);
    });



    it("response 200 and expected result for valid parameter '/lookup/gene/ENSG00000001561'", (done) => {
        jest.setTimeout(20000);

        const expectedRes = {
            "display_name": "ENPP4",
            "description": "ectonucleotide pyrophosphatase/phosphodiesterase 4",
            "stable_id": "ENSG00000001561",
            "seq_region_start": 46129989,
            "seq_region_end": 46146688,
            "seq_region_length": 16698,
            "transcripts": [
                {
                    "display_name": "ENPP4-201",
                    "stable_id": "ENST00000321037",
                    "seq_region_start": 46129989,
                    "seq_region_end": 46146688,
                    "seq_region_length": 16698,
                    "exons": [
                        {
                            "stable_id": "ENSE00001455147",
                            "seq_region_start": 46129989,
                            "seq_region_end": 46130189,
                            "seq_region_length": 199
                        },
                        {
                            "stable_id": "ENSE00001242068",
                            "seq_region_start": 46139551,
                            "seq_region_end": 46140409,
                            "seq_region_length": 857
                        },
                        {
                            "stable_id": "ENSE00000754692",
                            "seq_region_start": 46141052,
                            "seq_region_end": 46141222,
                            "seq_region_length": 169
                        },
                        {
                            "stable_id": "ENSE00001754265",
                            "seq_region_start": 46143276,
                            "seq_region_end": 46146688,
                            "seq_region_length": 3411
                        }
                    ]
                }
            ]
        };

        request(server)
            .get('/lookup/gene/ENSG00000001561')
            .expect(200, expectedRes, done);
    });





});





