
import { it, afterAll, beforeAll, describe } from '@jest/globals';
import { Server } from 'http';
import request from 'supertest';
import dotenv from 'dotenv';
import express from 'express';
import { Router } from '../src/routes/router';
import { Config } from '../src/config';


describe('lookup.routes.controller.test.internalFailure', () => {

    let server: Server;
    let env;


    beforeAll(() => {

        env = Object.assign({}, process.env);

        process.env.NODE_ENV = 'development';

        // Load the development configuration
        dotenv.config({ path: `./${process.env.NODE_ENV}.env` });

        // Force failure to load config 
        Config.getInstance.DB_CONFIG.DB_HOST = 'fail';

        const app: express.Application = express();

        const router = new Router(app);

        server = app.listen(3001, () => {
            console.log('listening on port 3001');
        });

    });

    afterAll((done) => {
        process.env = env;


        server.close(done);

    });


    it("response 500 and error for internal failure '/lookup/gene/ENSG00000001561'", (done) => {


        const expectedBody = {
            "error": {
                "message": "The data for the specified gene could not be retrieved"
            }
        };

        request(server)
            .get('/lookup/gene/ENSG00000001561/')
            .expect(500, expectedBody, done);

    });
});