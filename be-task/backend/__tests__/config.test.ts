

import { Config } from '../src/config';
import { ConfigException } from '../src/exceptions/configException';
import { it, expect, beforeEach, afterEach, describe } from '@jest/globals';


describe('Config', () => {
    let env;



    beforeEach(() => {
        env = Object.assign({}, process.env);

        process.env.NODE_ENV = 'development';
        process.env.PORT = '3000';
        process.env.DB_USERNAME = 'un';
        process.env.DB_HOST = 'host';
        process.env.DB_NAME = 'name';
        process.env.DB_PASSWORD = 'pw';
    });

    afterEach(() => {
        process.env = env;
    });



    it('should throw Error if NODE_ENV is not present in process.env', () => {
        const varName = 'NODE_ENV';

        delete process.env[varName];

        expect(() => {
            const config = Config.getInstance;
        }).toThrowError(new ConfigException(`${varName} env variable not defined`));
    });



    it('should throw Error if PORT is not present in process.env', () => {
        const varName = 'PORT';

        delete process.env[varName];

        expect(() => {
            const config = Config.getInstance;
        }).toThrowError(new ConfigException(`${varName} env variable not defined`));
    });




    it('should throw Error if DB_USERNAME is not present in process.env', () => {
        const varName = 'DB_USERNAME';

        delete process.env[varName];

        expect(() => {
            const config = Config.getInstance;
        }).toThrowError(new ConfigException(`${varName} env variable not defined`));
    });



    it('should throw Error if DB_HOST is not present in process.env', () => {
        const varName = 'DB_HOST';

        delete process.env[varName];

        expect(() => {
            const config = Config.getInstance;
        }).toThrowError(new ConfigException(`${varName} env variable not defined`));
    });




    it('should throw Error if DB_NAME is not present in process.env', () => {
        const varName = 'DB_NAME';

        delete process.env[varName];

        expect(() => {
            const config = Config.getInstance;
        }).toThrowError(new ConfigException(`${varName} env variable not defined`));
    });



    it('should throw Error if DB_PASSWORD is not present in process.env', () => {
        const varName = 'DB_PASSWORD';

        delete process.env[varName];

        expect(() => {
            const config = Config.getInstance;
        }).toThrowError(new ConfigException(`${varName} env variable not defined`));
    });



});
