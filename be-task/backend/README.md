# Backend Task

## Author

Izak Blom

izakjblom@gmail.com

## Table of Contents  

1. [Live Endpoint](#live-endpoint)   
2. [Libraries and Frameworks](#libraries-and-frameworks)
3. [API Documentation](#api-documentation)
4. [Run the App](#run-the-app)
5. [Related Questions Answers](#related-questions-answers)

## Live endpoint

A live endpoint URL for the API is available at:

<https://be-task-ebi-yzlm5zv5xa-nw.a.run.app/>

## Libraries and Frameworks

1. [NodeJS](https://nodejs.org/en/)
  Node allows running Javascript in the cross-platform Node runtime environment with high availibility due to asynchronous event loops and callbacks. The [npm](https://www.npmjs.com/) package manager provides a vast collection of third party libraries which can be used to enhance Node applications. A backend running Javascript allows for potential code-sharing between backend and frontend applications. 
2. [Express framework](https://expressjs.com/) for Node
   Minimalist framework for developing API services providing features such as middleware, request validation and routing.
3. [mysql](https://www.npmjs.com/package/mysql) npm package provides MySql integration for Node applications
4. [cors](https://www.npmjs.com/package/cors) provides Cross origin request middleware for Node Express applications and is used here since the frontend application is a single page application sending requests from a different origin endpoint than the backend endpoint.
5. [dotenv](https://www.npmjs.com/package/dotenv) npm package provides loading of environment variables into the running Node process and allows specification of variables in separate .env files which can be excluded from source control if some variables contain sensitive information. Also prevents "hard-coding" environment variables. 
6. [express-validator](https://www.npmjs.com/package/express-validator) npm package provides enhanced Express API request validation middleware. 
7. [swagger-jsdoc](https://www.npmjs.com/package/swagger-jsdoc) npm package provides inline JSDoc Swagger specifications (with `@swagger` annotations) which enhances the maintainability of Swagger documentation as code comments. 
8. [swagger-ui-express](https://www.npmjs.com/package/swagger-ui-express) npm package allows serving a Swagger UI as part of an Express application. 
9. Typescript provided by [typescript](https://www.npmjs.com/package/typescript) package allows the usage of Typescript which can be compiled to Javascript. Typescript allows type-safety and various language features not provided by normal Javascript.
10. [ts-node-dev](https://www.npmjs.com/package/ts-node-dev) npm package allows compilation of Typescript into javascript
11. Jest framework for unit testing provided through [jest](https://www.npmjs.com/package/jest) npm package. 
12. [supertest](https://www.npmjs.com/package/supertest) npm package allows HTTP assertions in Unit test implementations for NodeJS allowing API endpoint unit testing.
13. [cross-env](https://www.npmjs.com/package/cross-env) npm package allows cross-platform specification of environment variables in package.json

## API Documentation

The API is documented with [Swagger](https://swagger.io/). 

The complete interactive documentation can be viewed here: 

<https://be-task-ebi-yzlm5zv5xa-nw.a.run.app/> 

This Swagger documentation includes:

* All available API endpoints
* Descriptions for endpoint usage, responses and requests
* Example API requests and formats
* API Response schema definitions and formats
* API Response examples
* User interfaces to test each live API endpoint with custom input

## Run the App

### Option 1 - Run inside a Docker container (recommended)

1. `cd` into this directory
2. `docker-compose up -d` to run a detached container
3. Once the image has been built and runs in a local container (be_app_izak_blom), open <http://localhost:3000> in a browser window to view the running backend app's swagger documentation and to test the available endpoints
4. `docker-compose down` to terminate the running container once done

### Option 2 - Run natively for development

1. Ensure node and npm are installed on the local development environment <https://nodejs.org/en/download/> 
2. `cd` into this directory
3. `npm install`
4. `npm run dev`
5. Once the app has compiled and is running in the local terminal, navigate to <http://localhost:3000> in a browser window to view the running backend app's swagger documentation and to test the available endpoints 

### Environment configuration

Environment variables are provided in [development.env](./development.env). Ideally this file should be excluded from source control and the [example.env](./example.env) file shoud be used as a template to configure the environment variables. For the purpose of this assignment the file has been included. 

Providing different files for different "build-stages" can be done by adding a file named "build_stage_name.env" and filling in the values specified in [example.env](./example.env). When building the Docker image the `NODE_ENV` `ENV` variable should be set to build_stage_name used to specify the new .env file. 
Again, for the purpose of this exercise only one configuration file has been used.

**It is not necessary to modify the configuration to run this project successfully.**

The .env files are parsed by using the [dotenv](https://www.npmjs.com/package/dotenv) npm package. The file name to parse is set through the `NODE_ENV` environment variable.

## Related Questions Answers

### Deployment

#### Option 1: Docker container on a server machine

The application can be deployed on a target machine using the provided source code, Dockerfile and docker-compose.yml file. 

##### Scaling


**Vertical scaling:**

The [pm2](https://www.npmjs.com/package/pm2/) npm package  built with the docker image provides an automatic load balancer. It also allows you to "keep applications alive forever, to reload them without downtime and to facilitate common system admin tasks".

The `restart: always` flag specified in docker-compose.yml will automatically restart the service container in the event of any failure. 

The Docker Swarm orchestrator can additionally be used to deploy the application on multiple nodes. 

**Horizontal scaling:**

Increasing the provisioned hardware on the host virtual machine or the actual hardware on a machine running the app natively will achieve horizontal scaling. 

Steps:
1. Clone this repository on the target machine or copy this directory to the target machine
2. Install Docker Compose on the target machine (<https://docs.docker.com/compose/install/>)
3. `cd` into /path/to/backend/ on the target machine
4. Run `docker-compose up -d` to run the services on the machine
5. Ensure the correct ports are exposed for incoming traffic (Port 3000)

#### Option 2: Docker container on Google Cloud Platform

1. Setup a Google Cloud account: <https://cloud.google.com/gcp/getting-started>
2. Create a new project in the GCP console
3. Enable the Cloud Build API <https://console.cloud.google.com/cloud-build>
4. Go to GCP Identity and Access Management <https://console.cloud.google.com/access/iam> and assign the roles "Cloud Run Admin" and "Storage Object Admin" to the cloudbuild.gserviceaccount account
5. Install the Google Cloud SDK <https://cloud.google.com/sdk/docs/install> on your machine
6. Initialise the Google Cloud SDK with `gcloud init` and ensure your GCP Account is linked successfully
7. Build the application image and publish to GCP Container registry with `gcloud builds submit --tag gcr.io/your_gcp_project_id/be-task-ebi --project your_gcp_project_id`
8. View your image in the GCP Container registry <https://console.cloud.google.com/gcr>
9. Select the ellipsis on the latest image and select "Deploy to Cloud Run"
10. Setup the Cloud Run service to run the image by following the steps provided by the console.

##### Scaling

**Vertical scaling:**

Cloud Run is a fully managed serverless platform for running containers. The service auomatically scales running containers up and down according to traffic load and according to the maximum specified service instances. Details: <https://cloud.google.com/run>

**Horizontal scaling:**

The resources (CPU and RAM) for any Cloud Run service instance can be changed by simply deploying a new revision with an updated configuration and without any downtime.

### Testing

#### Unit tests

Unit tests are implemented in [./__tests__](./__tests__) using the [Jest framework](<https://jestjs.io/>). 

These tests can be run in the development environment with `npm test`. 

The endpoints provided by the API service are also tested as unit tests through the [supertest](https://www.npmjs.com/package/supertest) npm package which provides http assertions. 

#### Integration tests

**Database integration**:
Database integration is tested along with the unit tests thanks to the [supertest](https://www.npmjs.com/package/supertest) npm package. 

#### End-to-end tests

End-to-end tests can be implemented by creating a separate node project and utilising the [Jest framework](<https://jestjs.io/>). Test suites can be implemented to send actual HTTP requests to a live API endpoint to verify endpoint responses and behaviour. Running such a test project can be included as a build step in a deployment process, provided the actual API service is deployed prior to this step and in a "staging" environment. 
E2E tests will also enable consistent testing across multiple implementations of the backend API. 

#### Test Automation:

The Dockerfile contains the test command `npm test` as part of the image build procedure. This ensures tests are run whenever the image is rebuilt. Should any tests fail, the image build will also fail and consequently any deployments depending on the image build step will also fail. 

Similarly other tests suites (not implemented) can be run as part of a deployment script or as a CI/CD step on a tool such as Jenkins. 


### Documentation

The project includes a swagger specification which allows automatic generation of interactive API documentation. 

The Swagger documentation for this project can be seen here: 

<https://be-task-ebi-yzlm5zv5xa-nw.a.run.app/>

The [swagger-ui-express](https://www.npmjs.com/package/swagger-ui-express) npm package enables serving the Swagger interface as part of the Express web server application. 

The [swagger-jsdoc](https://www.npmjs.com/package/swagger-jsdoc) npm package allows declaring swagger specifications as inline code JSDoc comments by using the `@swagger` JSDoc annotation. This ensures the swagger specification remains up to date, provided the code comments are kept up to date as the implementation changes. An example of this inline swagger specification can be seen [here](./src/routes/lookup.routes.controller.ts). 

